﻿using System;

namespace ReduceButGrow
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Memanggil metode Grow dan mencetak hasilnya ke konsol
            Console.WriteLine(Grow(new int[] { 1, 2, 3, 4 }));
        }

        // Soal.
        // Given a non-empty array of integers, return the result of multiplying the values together in order.Example:
        //[1, 2, 3, 4] => 1 * 2 * 3 * 4 = 24

        // Metode untuk mengalikan semua elemen dalam array dan mengembalikan hasilnya
        public static int Grow(int[] x)
        {
            // Inisialisasi hasil dengan elemen pertama dari array
            int result = x[0];

            // Loop melalui elemen array dimulai dari indeks 1, karena elemen pertama sudah diambil
            for (int i = 1; i < x.Length; i++)
            {
                // Mengalikan hasil dengan elemen array saat ini
                result = result * x[i];
            }

            // Mengembalikan hasil perkalian semua elemen dalam array
            return result;
        }
    }
}
