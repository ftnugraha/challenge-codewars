﻿using System;
using System.Collections.Generic;

namespace ReversedSequence
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(ReverseSeq(5));
        }

        public static int[] ReverseSeq(int n)
        {
            int[] result = new int[n];

            for(int i = n; i >=1; i--) 
            {
                result[n - i] = i;
            }

            return result;
        }
    }
}
