﻿using System;

namespace GetCharFromASCII
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(GetChar(65)) ;
        }

        /*DESCRIPTION:
        Write a function which takes a number and returns the corresponding ASCII char for that value.*

        Example:
        65 --> 'A'
        97 --> 'a'
        48 --> '0'

        Link : https://www.codewars.com/kata/55ad04714f0b468e8200001c
        */

        //public static char GetChar(int charcode)
        //{
        //    //return (char)charcode ;
        //    return Convert.ToChar(charcode);
        //}

        public static char GetChar(int i) => (char) i;
    }
}
