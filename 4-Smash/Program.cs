﻿using System;
using System.Linq;

namespace _4_Smash
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Smash(new string[] { "this", "is", "a", "really", "long", "sentence" }));
            Console.WriteLine(Kata(new string[] { "hello", "world" }));
        }

        // Write a function that takes an array of words and smashes them together into a sentence and returns the sentence. You can ignore any need to sanitize words or add punctuation, but you should add spaces between each word. Be careful, there shouldn't be a space at the beginning or the end of the sentence!

        //Example
        //['hello', 'world', 'this', 'is', 'great']  =>  'hello world this is great'

        // https://www.codewars.com/kata/53dc23c68a0c93699800041d

        public static string Smash(string[] words)
        {
            // Looping
            //string result = "";

            //for(var i = 0; i < words.Length; i++)
            //{
            //    result += words[i];

            //    // Jika ketemu indeks yang terakhir, jangan kasih spasi
            //    if(i != words.Length - 1) result += " ";
            //}
            
            //return result;

            //Join
            return string.Join(" ", words);
        }

        public static string Kata(string[] words) => string.Join("-", words);
    }
}
