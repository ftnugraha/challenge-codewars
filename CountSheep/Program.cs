﻿using System;
using System.Threading.Channels;

namespace CountSheep
{
    internal class Program
    {
        static void Main(string[] args)
        {
            CountSheep(7);
        }

        /*Description Task:
        Given a non-negative integer, 3 for example, return a string with a murmur: "1 sheep...2 sheep...3 sheep...". Input will always be valid, i.e.no negative integers.

        Link : https://www.codewars.com/kata/5b077ebdaf15be5c7f000077
        */
        public static void CountSheep(int n)
        {
            string result = "";

            for(int i = 1; i <= n; i++)
            {
                result += ($"{i} sheep...");
            }

            Console.WriteLine(result);
        }
    }
}
