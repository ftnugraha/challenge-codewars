﻿using System;

namespace ItIsEven
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(IsEven(-4));
            Console.WriteLine("tes");
        }

        /*DESCRIPTION:
        In this Kata we are passing a number (n) into a function.

        Your code will determine if the number passed is even (or not).

        The function needs to return either a true or false.

        Numbers may be positive or negative, integers or floats.

        Floats with decimal part non equal to zero are considered UNeven for this kata.

        Link : https://www.codewars.com/kata/555a67db74814aa4ee0001b5
        */
        public static bool IsEven(double n)
        {
            // Your awesome code here!
            if(n % 2 == 0) return true;
            return false;
        }
    }
}
